-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 11, 2018 at 08:20 PM
-- Server version: 5.7.23-0ubuntu0.16.04.1
-- PHP Version: 7.1.22-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fiverr_tayyab_wedishedi`
--

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `city_name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `country_id`, `city_name`) VALUES
(1, 1, 'Islamabad'),
(2, 1, 'Karachi'),
(3, 1, 'Lahore'),
(4, 1, 'Abbaspur'),
(5, 1, 'Abbotabad'),
(6, 1, 'Abdul hakim'),
(7, 1, 'Adda Jahan Khan'),
(8, 1, 'Adda shaiwala'),
(9, 1, 'Akhora khattak'),
(10, 1, 'Ali chak'),
(11, 1, 'Alipur'),
(12, 1, 'Allahabad'),
(13, 1, 'Amangarh'),
(14, 1, 'Ambela'),
(15, 1, 'Arifwala'),
(16, 1, 'Attock'),
(17, 1, 'Babri banda'),
(18, 1, 'Badin'),
(19, 1, 'Bagh'),
(20, 1, 'Bahawalnagar'),
(21, 1, 'Bahawalpur'),
(22, 1, 'Bajaur'),
(23, 1, 'Balakot'),
(24, 1, 'Bannu'),
(25, 1, 'Barbar loi'),
(26, 1, 'Baroute'),
(27, 1, 'Bat khela'),
(28, 1, 'Battagram'),
(29, 1, 'Besham'),
(30, 1, 'Bewal'),
(31, 1, 'Bhai pheru'),
(32, 1, 'Bhakkar'),
(33, 1, 'Bhalwal'),
(34, 1, 'Bhan saeedabad'),
(35, 1, 'Bhara Kahu'),
(36, 1, 'Bhera'),
(37, 1, 'Bhimbar'),
(38, 1, 'Bhirya road'),
(39, 1, 'Bhuawana'),
(40, 1, 'Bisham'),
(41, 1, 'Blitang'),
(42, 1, 'Buchay \r\n\r\nkey'),
(43, 1, 'Bunner'),
(44, 1, 'Burewala'),
(45, 1, 'Chacklala'),
(46, 1, 'Chaghi'),
(47, 1, 'Chaininda'),
(48, 1, 'Chak 4 b c'),
(49, 1, 'Chak 46'),
(50, 1, 'Chak jamal'),
(51, 1, 'Chak jhumra'),
(52, 1, 'Chak sawara'),
(53, 1, 'Chak sheza'),
(54, 1, 'Chakwal'),
(55, 1, 'Chaman'),
(56, 1, 'Charsada'),
(57, 1, 'Chashma'),
(58, 1, 'Chawinda'),
(59, 1, 'Chicha watni'),
(60, 1, 'Chiniot'),
(61, 1, 'Chishtian'),
(62, 1, 'Chitral'),
(63, 1, 'Choa \r\n\r\nSaiden Shah'),
(64, 1, 'Chohar jamali'),
(65, 1, 'Choppar hatta'),
(66, 1, 'Chowk azam'),
(67, 1, 'Chowk mailta'),
(68, 1, 'Chowk munda'),
(69, 1, 'Chunian'),
(70, 1, 'Dadakhel'),
(71, 1, 'Dadu'),
(72, 1, 'Daharki'),
(73, 1, 'Dandot'),
(74, 1, 'Dargai'),
(75, 1, 'Darya Khan'),
(76, 1, 'Daska'),
(77, 1, 'Daud khel'),
(78, 1, 'Daulat pur'),
(79, 1, 'Daur'),
(80, 1, 'Deh pathaan'),
(81, 1, 'Depal pur'),
(82, 1, 'Dera Ghazi Khan'),
(83, 1, 'Dera Ismail \r\n\r\nKhan'),
(84, 1, 'Dera murad jamali'),
(85, 1, 'Dera nawab sahib'),
(86, 1, 'Dhatmal'),
(87, 1, 'Dhirkot'),
(88, 1, 'Dhoun kal'),
(89, 1, 'Digri'),
(90, 1, 'Dijkot'),
(91, 1, 'Dina'),
(92, 1, 'Dinga'),
(93, 1, 'Dir'),
(94, 1, 'Doaaba'),
(95, 1, 'Doltala'),
(96, 1, 'Domeli'),
(97, 1, 'Dudial'),
(98, 1, 'Dunyapur'),
(99, 1, 'Eminabad'),
(100, 1, 'Estate l.m factory'),
(101, 1, 'Faisalabad'),
(102, 1, 'Farooqabad'),
(103, 1, 'Fateh jang'),
(104, 1, 'Fateh \r\n\r\npur'),
(105, 1, 'Feroz walla'),
(106, 1, 'Feroz watan'),
(107, 1, 'Fizagat'),
(108, 1, 'FR Bannu / Lakki'),
(109, 1, 'FR Peshawar / Kohat'),
(110, 1, 'FR Tank / DI Khan'),
(111, 1, 'Gadoon amazai'),
(112, 1, 'Gaggo Mandi'),
(113, 1, 'Gakhar Mandi'),
(114, 1, 'Gambet'),
(115, 1, 'Garh maharaja'),
(116, 1, 'Garh more'),
(117, 1, 'Gari habibullah'),
(118, 1, 'Gari mori'),
(119, 1, 'Gawadar'),
(120, 1, 'Gharo'),
(121, 1, 'Ghazi'),
(122, 1, 'Ghotki'),
(123, 1, 'Ghuzdar'),
(124, 1, 'Gilgit'),
(125, 1, 'Gohar ghoushti'),
(126, 1, 'Gojra'),
(127, 1, 'Goular khel'),
(128, 1, 'Guddu'),
(129, 1, 'Gujar Khan'),
(130, 1, 'Gujranwala'),
(131, 1, 'Gujrat'),
(132, 1, 'Hafizabad'),
(133, 1, 'Hala'),
(134, 1, 'Hangu'),
(135, 1, 'Hari pur'),
(136, 1, 'Hariwala'),
(137, 1, 'Haroonabad'),
(138, 1, 'Hasilpur'),
(139, 1, 'Hassan abdal'),
(140, 1, 'Hattar'),
(141, 1, 'Hattian'),
(142, 1, 'Haveli Lakha'),
(143, 1, 'Havelian'),
(144, 1, 'Hayatabad'),
(145, 1, 'Hazro'),
(146, 1, 'Head marala'),
(147, 1, 'Hub Chowki'),
(148, 1, 'Hub inds \r\n\r\nestate'),
(149, 1, 'Hyderabad'),
(150, 1, 'Issa khel'),
(151, 1, 'Jaccobabad'),
(152, 1, 'Jaja abasian'),
(153, 1, 'Jalal pur jatan'),
(154, 1, 'Jalal pur priwala'),
(155, 1, 'Jampur'),
(156, 1, 'Jamrud road'),
(157, 1, 'Jamshoro'),
(158, 1, 'Jandanwala'),
(159, 1, 'Jaranwala'),
(160, 1, 'Jauharabad'),
(161, 1, 'Jehangira'),
(162, 1, 'Jehanian'),
(163, 1, 'Jehlum'),
(164, 1, 'Jhand'),
(165, 1, 'Jhang'),
(166, 1, 'Jhatta bhutta'),
(167, 1, 'Jhelum'),
(168, 1, 'Jhudo'),
(169, 1, 'Kabir wala'),
(170, 1, 'Kacha khooh'),
(171, 1, 'Kahuta'),
(172, 1, 'Kakul'),
(173, 1, 'Kakur \r\n\r\ntown'),
(174, 1, 'Kala bagh'),
(175, 1, 'Kala Shah kaku'),
(176, 1, 'Kalam'),
(177, 1, 'Kalar syedian'),
(178, 1, 'Kalaswala'),
(179, 1, 'Kallur kot'),
(180, 1, 'Kamalia'),
(181, 1, 'Kamalia musa'),
(182, 1, 'Kamber Ali Khan'),
(183, 1, 'Kamokey'),
(184, 1, 'Kamra'),
(185, 1, 'Kandh kot'),
(186, 1, 'Kandiaro'),
(187, 1, 'Karak'),
(188, 1, 'Karoor pacca'),
(189, 1, 'Karore lalisan'),
(190, 1, 'Kashmir'),
(191, 1, 'Kashmore'),
(192, 1, 'Kasur'),
(193, 1, 'Kazi ahmed'),
(194, 1, 'Khair pur'),
(195, 1, 'Khair pur mir'),
(196, 1, 'Khairpur Nathan Shah'),
(197, 1, 'Khanbel'),
(198, 1, 'Khandabad'),
(199, 1, 'Khanewal'),
(200, 1, 'Khangah Sharif'),
(201, 1, 'Khangarh'),
(202, 1, 'Khanpur'),
(203, 1, 'Khanqah dogran'),
(204, 1, 'Khanqah sharif'),
(205, 1, 'Kharan'),
(206, 1, 'Kharian'),
(207, 1, 'Khewra'),
(208, 1, 'Khoski'),
(209, 1, 'Khuiratta'),
(210, 1, 'Khurian wala'),
(211, 1, 'Khushab'),
(212, 1, 'Khushal kot'),
(213, 1, 'Khuzdar'),
(214, 1, 'Killa Saifullah'),
(215, 1, 'Kohat'),
(216, 1, 'Kohistan'),
(217, 1, 'Kot Addu'),
(218, 1, 'Kot bunglow'),
(219, 1, 'Kot ghulam mohd'),
(220, 1, 'Kot mithan'),
(221, 1, 'Kot radha \r\n\r\nkishan'),
(222, 1, 'Kotla'),
(223, 1, 'Kotla arab ali khan'),
(224, 1, 'Kotla jam'),
(225, 1, 'Kotla Pathan'),
(226, 1, 'Kotli'),
(227, 1, 'Kotli loharan'),
(228, 1, 'Kotri'),
(229, 1, 'Kumbh'),
(230, 1, 'Kundina'),
(231, 1, 'Kunjah'),
(232, 1, 'Kunri'),
(233, 1, 'Kurram Agency'),
(234, 1, 'Lakimarwat'),
(235, 1, 'Lala rukh'),
(236, 1, 'Lalamusa'),
(237, 1, 'Laliah'),
(238, 1, 'Lalshanra'),
(239, 1, 'Larkana'),
(240, 1, 'Lasbela'),
(241, 1, 'Lawrence \r\n\r\npur'),
(242, 1, 'Layyah'),
(243, 1, 'Leepa'),
(244, 1, 'Liaquat pur'),
(245, 1, 'Lodhran'),
(246, 1, 'Loralai'),
(247, 1, 'Lower Dir'),
(248, 1, 'Ludhan'),
(249, 1, 'Machi goth'),
(250, 1, 'Madina'),
(251, 1, 'Mailsi'),
(252, 1, 'Makli'),
(253, 1, 'Makran'),
(254, 1, 'Malakand'),
(255, 1, 'Malakwal'),
(256, 1, 'Mamu kunjan'),
(257, 1, 'Mandi \r\n\r\nBahauddin'),
(258, 1, 'Mandi Faizabad'),
(259, 1, 'Mandra'),
(260, 1, 'Manga Mandi'),
(261, 1, 'Mangal sada'),
(262, 1, 'Mangi'),
(263, 1, 'Mangla'),
(264, 1, 'Mangowal'),
(265, 1, 'Manoabad'),
(266, 1, 'Mansehra'),
(267, 1, 'Mardan'),
(268, 1, 'Mari indus'),
(269, 1, 'Mastoi'),
(270, 1, 'Matiari'),
(271, 1, 'Matli'),
(272, 1, 'Mehar'),
(273, 1, 'Mehmood kot'),
(274, 1, 'Mehrab pur'),
(275, 1, 'Mian Chunnu'),
(276, 1, 'Mian Walli'),
(277, 1, 'Mingora'),
(278, 1, 'Mir ali'),
(279, 1, 'Miran Shah'),
(280, 1, 'Mirpur'),
(281, 1, 'Mirpur Khas'),
(282, 1, 'Mirpur Mathelo'),
(283, 1, 'Mohen jo daro'),
(284, 1, 'Mohmand'),
(285, 1, 'More kunda'),
(286, 1, 'Morgah'),
(287, 1, 'Moro'),
(288, 1, 'Mubarik pur'),
(289, 1, 'Multan'),
(290, 1, 'Muridkay'),
(291, 1, 'Murree'),
(292, 1, 'Musafir khana'),
(293, 1, 'Musakhel'),
(294, 1, 'Mustang'),
(295, 1, 'Muzaffarabad'),
(296, 1, 'Muzaffargarh'),
(297, 1, 'Nankana Sahib'),
(298, 1, 'Narang Mandi'),
(299, 1, 'Narowal'),
(300, 1, 'Naseerabad'),
(301, 1, 'Naudero'),
(302, 1, 'Naukot'),
(303, 1, 'Naukundi'),
(304, 1, 'Nawab Shah'),
(305, 1, 'New saeedabad'),
(306, 1, 'Nilam'),
(307, 1, 'Nilore'),
(308, 1, 'Noor kot'),
(309, 1, 'Nooriabad'),
(310, 1, 'Noorpur nooranga'),
(311, 1, 'North Wazirstan'),
(312, 1, 'Noshki'),
(313, 1, 'Nowshera'),
(314, 1, 'Nowshera Cantt'),
(315, 1, 'Nowshero Feroz'),
(316, 1, 'Okara'),
(317, 1, 'Orakzai'),
(318, 1, 'Padidan'),
(319, 1, 'Pak china fertilizer'),
(320, 1, 'Pak pattan \r\n\r\nsharif'),
(321, 1, 'Panjan kisan'),
(322, 1, 'Panjgoor'),
(323, 1, 'Panjgur'),
(324, 1, 'Pannu aqil'),
(325, 1, 'Pasni'),
(326, 1, 'Pasroor'),
(327, 1, 'Patika'),
(328, 1, 'Patoki'),
(329, 1, 'Peshawar'),
(330, 1, 'Phagwar'),
(331, 1, 'Phalia'),
(332, 1, 'Phool nagar'),
(333, 1, 'Piaro goth'),
(334, 1, 'Pind Dadan Khan'),
(335, 1, 'Pindi \r\n\r\nBhattian'),
(336, 1, 'Pindi bhohri'),
(337, 1, 'Pindi gheb'),
(338, 1, 'Pir mahal'),
(339, 1, 'Pirpai'),
(340, 1, 'Pishin'),
(341, 1, 'Punch'),
(342, 1, 'Punjgor'),
(343, 1, 'Qalandarabad'),
(344, 1, 'Qambar'),
(345, 1, 'Qambar \r\n\r\nShahdatkot'),
(346, 1, 'Qasba gujrat'),
(347, 1, 'Qazi ahmed'),
(348, 1, 'Quaidabad'),
(349, 1, 'Quetta'),
(350, 1, 'Rabwah'),
(351, 1, 'Rahimyar khan'),
(352, 1, 'Rahwali'),
(353, 1, 'Raiwind'),
(354, 1, 'Rajana'),
(355, 1, 'Rajanpur'),
(356, 1, 'Rangoo'),
(357, 1, 'Ranipur'),
(358, 1, 'Ratto dero'),
(359, 1, 'Rawala kot'),
(360, 1, 'Rawalpindi'),
(361, 1, 'Rawat'),
(362, 1, 'Renala khurd'),
(363, 1, 'Risalpur'),
(364, 1, 'Rohri'),
(365, 1, 'Sadiqabad'),
(366, 1, 'Sagri'),
(367, 1, 'Sahiwal'),
(368, 1, 'Saidu sharif'),
(369, 1, 'Sajawal'),
(370, 1, 'Sakrand'),
(371, 1, 'Samanabad'),
(372, 1, 'Sambrial'),
(373, 1, 'Samma satta'),
(374, 1, 'Samundri'),
(375, 1, 'Sanghar'),
(376, 1, 'Sanghi'),
(377, 1, 'Sangla hill'),
(378, 1, 'Sangote'),
(379, 1, 'Sanjwal'),
(380, 1, 'Sara e \r\n\r\nalamgir'),
(381, 1, 'Sara e naurang'),
(382, 1, 'Sargodha'),
(383, 1, 'Satyana bangla'),
(384, 1, 'Sehar baqlas'),
(385, 1, 'Serai alamgir'),
(386, 1, 'Shadiwal'),
(387, 1, 'Shahdad Kot'),
(388, 1, 'Shahdad Pur'),
(389, 1, 'Shaheed Benazirabad'),
(390, 1, 'Shahkot'),
(391, 1, 'Shahpur Chakar'),
(392, 1, 'Shaikhupura'),
(393, 1, 'Shakargarh'),
(394, 1, 'Shamsabad'),
(395, 1, 'Shangla'),
(396, 1, 'Shankiari'),
(397, 1, 'Shedani sharif'),
(398, 1, 'Sheikhupura'),
(399, 1, 'Shemier'),
(400, 1, 'Shikar pur'),
(401, 1, 'Shorkot'),
(402, 1, 'Shujabad'),
(403, 1, 'Sialkot'),
(404, 1, 'Sibi'),
(405, 1, 'Sidhnoti'),
(406, 1, 'Sihala'),
(407, 1, 'Sikandarabad'),
(408, 1, 'Silanwala'),
(409, 1, 'Sita road'),
(410, 1, 'Skardu'),
(411, 1, 'Sohawa district daska'),
(412, 1, 'Sohawa \r\n\r\ndistrict jelum'),
(413, 1, 'South Wazirstan'),
(414, 1, 'Sudhnoti'),
(415, 1, 'Sukkur'),
(416, 1, 'Swabi'),
(417, 1, 'Swat'),
(418, 1, 'Swatmingora'),
(419, 1, 'Takhtbai'),
(420, 1, 'Talagang'),
(421, 1, 'Talamba'),
(422, 1, 'Talhur'),
(423, 1, 'Tando adam'),
(424, 1, 'Tando allahyar'),
(425, 1, 'Tando jam'),
(426, 1, 'Tando mohd khan'),
(427, 1, 'Tank'),
(428, 1, 'Tarbela'),
(429, 1, 'Tarmatan'),
(430, 1, 'Tarnol'),
(431, 1, 'Taunsa sharif'),
(432, 1, 'Taxila'),
(433, 1, 'Tharo Shah'),
(434, 1, 'Tharparkar'),
(435, 1, 'Thatta'),
(436, 1, 'Theing jattan more'),
(437, 1, 'Thull'),
(438, 1, 'Tibba sultanpur'),
(439, 1, 'Timergara'),
(440, 1, 'Tobatek singh'),
(441, 1, 'Topi'),
(442, 1, 'Toru'),
(443, 1, 'Trinda mohd pannah'),
(444, 1, 'Turbat'),
(445, 1, 'Ubaro'),
(446, 1, 'Ugoki'),
(447, 1, 'Ukba'),
(448, 1, 'Umar kot'),
(449, 1, 'Upper deval'),
(450, 1, 'Upper Dir'),
(451, 1, 'Usta mohammad'),
(452, 1, 'Utror'),
(453, 1, 'Vehari'),
(454, 1, 'Village Sunder'),
(455, 1, 'Wah cantt'),
(456, 1, 'Wahi hassain'),
(457, 1, 'Wan radha ram'),
(458, 1, 'Warah'),
(459, 1, 'Warburton'),
(460, 1, 'Washuk'),
(461, 1, 'Wazirabad'),
(462, 1, 'Yazman Mandi'),
(463, 1, 'Zahir pir'),
(464, 1, 'Zhob'),
(465, 1, 'Ziarat'),
(466, 0, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `Event_id` int(11) NOT NULL,
  `Event_title` varchar(128) DEFAULT NULL,
  `Event_type` int(11) DEFAULT NULL,
  `Event_desc` text,
  `Event_start_date` datetime DEFAULT NULL,
  `Event_end_date` datetime DEFAULT NULL,
  `Event_expected_guest` varchar(50) DEFAULT NULL,
  `CreatedBy` int(11) DEFAULT NULL,
  `Created_datetime` datetime DEFAULT NULL,
  `Modified_by` int(11) DEFAULT NULL,
  `Modified_date` datetime DEFAULT NULL,
  `Is_Active` bit(1) DEFAULT b'1',
  `User_id` int(11) DEFAULT NULL,
  `Vendor_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`Event_id`, `Event_title`, `Event_type`, `Event_desc`, `Event_start_date`, `Event_end_date`, `Event_expected_guest`, `CreatedBy`, `Created_datetime`, `Modified_by`, `Modified_date`, `Is_Active`, `User_id`, `Vendor_id`) VALUES
(1, NULL, NULL, NULL, '2018-10-11 19:47:54', '2018-10-11 19:47:54', NULL, 1, '2018-10-11 19:47:54', NULL, NULL, b'1', 1, 1),
(2, NULL, NULL, NULL, '2018-10-11 19:47:59', '2018-10-11 19:47:59', NULL, 1, '2018-10-11 19:47:59', NULL, NULL, b'1', 1, 1),
(3, 'test', 0, 'test', '2018-10-17 19:50:53', '2018-10-18 19:50:53', 'test', 1, '2018-10-11 19:50:53', NULL, NULL, b'1', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `faq_id` int(10) UNSIGNED NOT NULL,
  `faq_title` varchar(50) DEFAULT NULL,
  `Vendor_type_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`faq_id`, `faq_title`, `Vendor_type_id`) VALUES
(1, 'How much minimum people can accomodate?', 1),
(2, 'How much maximum people can accomodate?', 1),
(3, 'Do you have a hall or outdoor places\r?', 1),
(4, 'Do you have a parking space\r\n?', 1),
(5, 'Do you have restrooms/washrooms?', 1),
(6, 'Is ther any room for bride?', 1),
(7, 'Do you offer tenting service?', 1),
(8, 'Any type of cancellation policy?', 1),
(9, 'Do you have First Aid Box?', 1),
(10, 'Do you have a Food Authority license?', 4),
(11, 'Rate your Food Quality between 1-5? \r\n', 4),
(12, 'Ratio of servers to guest\r\n?', 4),
(13, 'Any type of Cancellation Policy?', 4),
(14, 'What Type of Music of you play?', 3),
(15, 'Any smoke or lighting effects\r\n?', 3),
(16, 'How much hours included in package? \r\n', 3),
(17, 'Any backup plan in case of equipement malfunction?', 3),
(18, 'Any type of Cancellation Policy?', 3),
(19, 'Do you work on user location or yours?\r\n', 7),
(20, 'Any type of Cancellation Policy?', 7),
(21, 'How much guest will come out with user?', 7),
(22, 'Do you have First Aid Box?', 7),
(23, 'Do you charge per milage or hour?', 9),
(24, 'Any type of Cancellation Policy?', 9),
(25, 'Does your Car has an auto Insurance\r\n?', 9),
(26, 'Is the Driver available?\r\n', 9),
(27, 'Do you have First Aid Box?', 9),
(28, 'How much time you need to setup?', 2),
(29, 'Any smoke or lighting effects\r\n?', 2),
(30, 'How much hours included in package? \r\n', 2),
(31, 'Any backup plan in case of equipement malfunction?', 2),
(32, 'Any type of Cancellation Policy?', 2),
(33, 'Do you offer fresh flowers?\r\n', 2),
(34, 'What is your camera quality?', 5),
(35, 'Did you just Photoshoot Or Film?', 5),
(36, 'Are you a Videographer?', 5),
(37, 'Photography style (traditional, photojournalistic)', 5),
(38, 'Any backup plan in case of equipement malfunction?', 5),
(39, 'Deliver of photos with album in no of days?', 5),
(40, 'Copyright issue.\r\n', 5);

-- --------------------------------------------------------

--
-- Table structure for table `lb_city`
--

CREATE TABLE `lb_city` (
  `city_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `city_name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lb_city`
--

INSERT INTO `lb_city` (`city_id`, `country_id`, `city_name`) VALUES
(1, 1, 'Islamabad'),
(2, 1, 'Karachi'),
(3, 1, 'Lahore'),
(4, 1, 'Abbaspur'),
(5, 1, 'Abbotabad'),
(6, 1, 'Abdul hakim'),
(7, 1, 'Adda Jahan Khan'),
(8, 1, 'Adda shaiwala'),
(9, 1, 'Akhora khattak'),
(10, 1, 'Ali chak'),
(11, 1, 'Alipur'),
(12, 1, 'Allahabad'),
(13, 1, 'Amangarh'),
(14, 1, 'Ambela'),
(15, 1, 'Arifwala'),
(16, 1, 'Attock'),
(17, 1, 'Babri banda'),
(18, 1, 'Badin'),
(19, 1, 'Bagh'),
(20, 1, 'Bahawalnagar'),
(21, 1, 'Bahawalpur'),
(22, 1, 'Bajaur'),
(23, 1, 'Balakot'),
(24, 1, 'Bannu'),
(25, 1, 'Barbar loi'),
(26, 1, 'Baroute'),
(27, 1, 'Bat khela'),
(28, 1, 'Battagram'),
(29, 1, 'Besham'),
(30, 1, 'Bewal'),
(31, 1, 'Bhai pheru'),
(32, 1, 'Bhakkar'),
(33, 1, 'Bhalwal'),
(34, 1, 'Bhan saeedabad'),
(35, 1, 'Bhara Kahu'),
(36, 1, 'Bhera'),
(37, 1, 'Bhimbar'),
(38, 1, 'Bhirya road'),
(39, 1, 'Bhuawana'),
(40, 1, 'Bisham'),
(41, 1, 'Blitang'),
(42, 1, 'Buchay \r\n\r\nkey'),
(43, 1, 'Bunner'),
(44, 1, 'Burewala'),
(45, 1, 'Chacklala'),
(46, 1, 'Chaghi'),
(47, 1, 'Chaininda'),
(48, 1, 'Chak 4 b c'),
(49, 1, 'Chak 46'),
(50, 1, 'Chak jamal'),
(51, 1, 'Chak jhumra'),
(52, 1, 'Chak sawara'),
(53, 1, 'Chak sheza'),
(54, 1, 'Chakwal'),
(55, 1, 'Chaman'),
(56, 1, 'Charsada'),
(57, 1, 'Chashma'),
(58, 1, 'Chawinda'),
(59, 1, 'Chicha watni'),
(60, 1, 'Chiniot'),
(61, 1, 'Chishtian'),
(62, 1, 'Chitral'),
(63, 1, 'Choa \r\n\r\nSaiden Shah'),
(64, 1, 'Chohar jamali'),
(65, 1, 'Choppar hatta'),
(66, 1, 'Chowk azam'),
(67, 1, 'Chowk mailta'),
(68, 1, 'Chowk munda'),
(69, 1, 'Chunian'),
(70, 1, 'Dadakhel'),
(71, 1, 'Dadu'),
(72, 1, 'Daharki'),
(73, 1, 'Dandot'),
(74, 1, 'Dargai'),
(75, 1, 'Darya Khan'),
(76, 1, 'Daska'),
(77, 1, 'Daud khel'),
(78, 1, 'Daulat pur'),
(79, 1, 'Daur'),
(80, 1, 'Deh pathaan'),
(81, 1, 'Depal pur'),
(82, 1, 'Dera Ghazi Khan'),
(83, 1, 'Dera Ismail \r\n\r\nKhan'),
(84, 1, 'Dera murad jamali'),
(85, 1, 'Dera nawab sahib'),
(86, 1, 'Dhatmal'),
(87, 1, 'Dhirkot'),
(88, 1, 'Dhoun kal'),
(89, 1, 'Digri'),
(90, 1, 'Dijkot'),
(91, 1, 'Dina'),
(92, 1, 'Dinga'),
(93, 1, 'Dir'),
(94, 1, 'Doaaba'),
(95, 1, 'Doltala'),
(96, 1, 'Domeli'),
(97, 1, 'Dudial'),
(98, 1, 'Dunyapur'),
(99, 1, 'Eminabad'),
(100, 1, 'Estate l.m factory'),
(101, 1, 'Faisalabad'),
(102, 1, 'Farooqabad'),
(103, 1, 'Fateh jang'),
(104, 1, 'Fateh \r\n\r\npur'),
(105, 1, 'Feroz walla'),
(106, 1, 'Feroz watan'),
(107, 1, 'Fizagat'),
(108, 1, 'FR Bannu / Lakki'),
(109, 1, 'FR Peshawar / Kohat'),
(110, 1, 'FR Tank / DI Khan'),
(111, 1, 'Gadoon amazai'),
(112, 1, 'Gaggo Mandi'),
(113, 1, 'Gakhar Mandi'),
(114, 1, 'Gambet'),
(115, 1, 'Garh maharaja'),
(116, 1, 'Garh more'),
(117, 1, 'Gari habibullah'),
(118, 1, 'Gari mori'),
(119, 1, 'Gawadar'),
(120, 1, 'Gharo'),
(121, 1, 'Ghazi'),
(122, 1, 'Ghotki'),
(123, 1, 'Ghuzdar'),
(124, 1, 'Gilgit'),
(125, 1, 'Gohar ghoushti'),
(126, 1, 'Gojra'),
(127, 1, 'Goular khel'),
(128, 1, 'Guddu'),
(129, 1, 'Gujar Khan'),
(130, 1, 'Gujranwala'),
(131, 1, 'Gujrat'),
(132, 1, 'Hafizabad'),
(133, 1, 'Hala'),
(134, 1, 'Hangu'),
(135, 1, 'Hari pur'),
(136, 1, 'Hariwala'),
(137, 1, 'Haroonabad'),
(138, 1, 'Hasilpur'),
(139, 1, 'Hassan abdal'),
(140, 1, 'Hattar'),
(141, 1, 'Hattian'),
(142, 1, 'Haveli Lakha'),
(143, 1, 'Havelian'),
(144, 1, 'Hayatabad'),
(145, 1, 'Hazro'),
(146, 1, 'Head marala'),
(147, 1, 'Hub Chowki'),
(148, 1, 'Hub inds \r\n\r\nestate'),
(149, 1, 'Hyderabad'),
(150, 1, 'Issa khel'),
(151, 1, 'Jaccobabad'),
(152, 1, 'Jaja abasian'),
(153, 1, 'Jalal pur jatan'),
(154, 1, 'Jalal pur priwala'),
(155, 1, 'Jampur'),
(156, 1, 'Jamrud road'),
(157, 1, 'Jamshoro'),
(158, 1, 'Jandanwala'),
(159, 1, 'Jaranwala'),
(160, 1, 'Jauharabad'),
(161, 1, 'Jehangira'),
(162, 1, 'Jehanian'),
(163, 1, 'Jehlum'),
(164, 1, 'Jhand'),
(165, 1, 'Jhang'),
(166, 1, 'Jhatta bhutta'),
(167, 1, 'Jhelum'),
(168, 1, 'Jhudo'),
(169, 1, 'Kabir wala'),
(170, 1, 'Kacha khooh'),
(171, 1, 'Kahuta'),
(172, 1, 'Kakul'),
(173, 1, 'Kakur \r\n\r\ntown'),
(174, 1, 'Kala bagh'),
(175, 1, 'Kala Shah kaku'),
(176, 1, 'Kalam'),
(177, 1, 'Kalar syedian'),
(178, 1, 'Kalaswala'),
(179, 1, 'Kallur kot'),
(180, 1, 'Kamalia'),
(181, 1, 'Kamalia musa'),
(182, 1, 'Kamber Ali Khan'),
(183, 1, 'Kamokey'),
(184, 1, 'Kamra'),
(185, 1, 'Kandh kot'),
(186, 1, 'Kandiaro'),
(187, 1, 'Karak'),
(188, 1, 'Karoor pacca'),
(189, 1, 'Karore lalisan'),
(190, 1, 'Kashmir'),
(191, 1, 'Kashmore'),
(192, 1, 'Kasur'),
(193, 1, 'Kazi ahmed'),
(194, 1, 'Khair pur'),
(195, 1, 'Khair pur mir'),
(196, 1, 'Khairpur Nathan Shah'),
(197, 1, 'Khanbel'),
(198, 1, 'Khandabad'),
(199, 1, 'Khanewal'),
(200, 1, 'Khangah Sharif'),
(201, 1, 'Khangarh'),
(202, 1, 'Khanpur'),
(203, 1, 'Khanqah dogran'),
(204, 1, 'Khanqah sharif'),
(205, 1, 'Kharan'),
(206, 1, 'Kharian'),
(207, 1, 'Khewra'),
(208, 1, 'Khoski'),
(209, 1, 'Khuiratta'),
(210, 1, 'Khurian wala'),
(211, 1, 'Khushab'),
(212, 1, 'Khushal kot'),
(213, 1, 'Khuzdar'),
(214, 1, 'Killa Saifullah'),
(215, 1, 'Kohat'),
(216, 1, 'Kohistan'),
(217, 1, 'Kot Addu'),
(218, 1, 'Kot bunglow'),
(219, 1, 'Kot ghulam mohd'),
(220, 1, 'Kot mithan'),
(221, 1, 'Kot radha \r\n\r\nkishan'),
(222, 1, 'Kotla'),
(223, 1, 'Kotla arab ali khan'),
(224, 1, 'Kotla jam'),
(225, 1, 'Kotla Pathan'),
(226, 1, 'Kotli'),
(227, 1, 'Kotli loharan'),
(228, 1, 'Kotri'),
(229, 1, 'Kumbh'),
(230, 1, 'Kundina'),
(231, 1, 'Kunjah'),
(232, 1, 'Kunri'),
(233, 1, 'Kurram Agency'),
(234, 1, 'Lakimarwat'),
(235, 1, 'Lala rukh'),
(236, 1, 'Lalamusa'),
(237, 1, 'Laliah'),
(238, 1, 'Lalshanra'),
(239, 1, 'Larkana'),
(240, 1, 'Lasbela'),
(241, 1, 'Lawrence \r\n\r\npur'),
(242, 1, 'Layyah'),
(243, 1, 'Leepa'),
(244, 1, 'Liaquat pur'),
(245, 1, 'Lodhran'),
(246, 1, 'Loralai'),
(247, 1, 'Lower Dir'),
(248, 1, 'Ludhan'),
(249, 1, 'Machi goth'),
(250, 1, 'Madina'),
(251, 1, 'Mailsi'),
(252, 1, 'Makli'),
(253, 1, 'Makran'),
(254, 1, 'Malakand'),
(255, 1, 'Malakwal'),
(256, 1, 'Mamu kunjan'),
(257, 1, 'Mandi \r\n\r\nBahauddin'),
(258, 1, 'Mandi Faizabad'),
(259, 1, 'Mandra'),
(260, 1, 'Manga Mandi'),
(261, 1, 'Mangal sada'),
(262, 1, 'Mangi'),
(263, 1, 'Mangla'),
(264, 1, 'Mangowal'),
(265, 1, 'Manoabad'),
(266, 1, 'Mansehra'),
(267, 1, 'Mardan'),
(268, 1, 'Mari indus'),
(269, 1, 'Mastoi'),
(270, 1, 'Matiari'),
(271, 1, 'Matli'),
(272, 1, 'Mehar'),
(273, 1, 'Mehmood kot'),
(274, 1, 'Mehrab pur'),
(275, 1, 'Mian Chunnu'),
(276, 1, 'Mian Walli'),
(277, 1, 'Mingora'),
(278, 1, 'Mir ali'),
(279, 1, 'Miran Shah'),
(280, 1, 'Mirpur'),
(281, 1, 'Mirpur Khas'),
(282, 1, 'Mirpur Mathelo'),
(283, 1, 'Mohen jo daro'),
(284, 1, 'Mohmand'),
(285, 1, 'More kunda'),
(286, 1, 'Morgah'),
(287, 1, 'Moro'),
(288, 1, 'Mubarik pur'),
(289, 1, 'Multan'),
(290, 1, 'Muridkay'),
(291, 1, 'Murree'),
(292, 1, 'Musafir khana'),
(293, 1, 'Musakhel'),
(294, 1, 'Mustang'),
(295, 1, 'Muzaffarabad'),
(296, 1, 'Muzaffargarh'),
(297, 1, 'Nankana Sahib'),
(298, 1, 'Narang Mandi'),
(299, 1, 'Narowal'),
(300, 1, 'Naseerabad'),
(301, 1, 'Naudero'),
(302, 1, 'Naukot'),
(303, 1, 'Naukundi'),
(304, 1, 'Nawab Shah'),
(305, 1, 'New saeedabad'),
(306, 1, 'Nilam'),
(307, 1, 'Nilore'),
(308, 1, 'Noor kot'),
(309, 1, 'Nooriabad'),
(310, 1, 'Noorpur nooranga'),
(311, 1, 'North Wazirstan'),
(312, 1, 'Noshki'),
(313, 1, 'Nowshera'),
(314, 1, 'Nowshera Cantt'),
(315, 1, 'Nowshero Feroz'),
(316, 1, 'Okara'),
(317, 1, 'Orakzai'),
(318, 1, 'Padidan'),
(319, 1, 'Pak china fertilizer'),
(320, 1, 'Pak pattan \r\n\r\nsharif'),
(321, 1, 'Panjan kisan'),
(322, 1, 'Panjgoor'),
(323, 1, 'Panjgur'),
(324, 1, 'Pannu aqil'),
(325, 1, 'Pasni'),
(326, 1, 'Pasroor'),
(327, 1, 'Patika'),
(328, 1, 'Patoki'),
(329, 1, 'Peshawar'),
(330, 1, 'Phagwar'),
(331, 1, 'Phalia'),
(332, 1, 'Phool nagar'),
(333, 1, 'Piaro goth'),
(334, 1, 'Pind Dadan Khan'),
(335, 1, 'Pindi \r\n\r\nBhattian'),
(336, 1, 'Pindi bhohri'),
(337, 1, 'Pindi gheb'),
(338, 1, 'Pir mahal'),
(339, 1, 'Pirpai'),
(340, 1, 'Pishin'),
(341, 1, 'Punch'),
(342, 1, 'Punjgor'),
(343, 1, 'Qalandarabad'),
(344, 1, 'Qambar'),
(345, 1, 'Qambar \r\n\r\nShahdatkot'),
(346, 1, 'Qasba gujrat'),
(347, 1, 'Qazi ahmed'),
(348, 1, 'Quaidabad'),
(349, 1, 'Quetta'),
(350, 1, 'Rabwah'),
(351, 1, 'Rahimyar khan'),
(352, 1, 'Rahwali'),
(353, 1, 'Raiwind'),
(354, 1, 'Rajana'),
(355, 1, 'Rajanpur'),
(356, 1, 'Rangoo'),
(357, 1, 'Ranipur'),
(358, 1, 'Ratto dero'),
(359, 1, 'Rawala kot'),
(360, 1, 'Rawalpindi'),
(361, 1, 'Rawat'),
(362, 1, 'Renala khurd'),
(363, 1, 'Risalpur'),
(364, 1, 'Rohri'),
(365, 1, 'Sadiqabad'),
(366, 1, 'Sagri'),
(367, 1, 'Sahiwal'),
(368, 1, 'Saidu sharif'),
(369, 1, 'Sajawal'),
(370, 1, 'Sakrand'),
(371, 1, 'Samanabad'),
(372, 1, 'Sambrial'),
(373, 1, 'Samma satta'),
(374, 1, 'Samundri'),
(375, 1, 'Sanghar'),
(376, 1, 'Sanghi'),
(377, 1, 'Sangla hill'),
(378, 1, 'Sangote'),
(379, 1, 'Sanjwal'),
(380, 1, 'Sara e \r\n\r\nalamgir'),
(381, 1, 'Sara e naurang'),
(382, 1, 'Sargodha'),
(383, 1, 'Satyana bangla'),
(384, 1, 'Sehar baqlas'),
(385, 1, 'Serai alamgir'),
(386, 1, 'Shadiwal'),
(387, 1, 'Shahdad Kot'),
(388, 1, 'Shahdad Pur'),
(389, 1, 'Shaheed Benazirabad'),
(390, 1, 'Shahkot'),
(391, 1, 'Shahpur Chakar'),
(392, 1, 'Shaikhupura'),
(393, 1, 'Shakargarh'),
(394, 1, 'Shamsabad'),
(395, 1, 'Shangla'),
(396, 1, 'Shankiari'),
(397, 1, 'Shedani sharif'),
(398, 1, 'Sheikhupura'),
(399, 1, 'Shemier'),
(400, 1, 'Shikar pur'),
(401, 1, 'Shorkot'),
(402, 1, 'Shujabad'),
(403, 1, 'Sialkot'),
(404, 1, 'Sibi'),
(405, 1, 'Sidhnoti'),
(406, 1, 'Sihala'),
(407, 1, 'Sikandarabad'),
(408, 1, 'Silanwala'),
(409, 1, 'Sita road'),
(410, 1, 'Skardu'),
(411, 1, 'Sohawa district daska'),
(412, 1, 'Sohawa \r\n\r\ndistrict jelum'),
(413, 1, 'South Wazirstan'),
(414, 1, 'Sudhnoti'),
(415, 1, 'Sukkur'),
(416, 1, 'Swabi'),
(417, 1, 'Swat'),
(418, 1, 'Swatmingora'),
(419, 1, 'Takhtbai'),
(420, 1, 'Talagang'),
(421, 1, 'Talamba'),
(422, 1, 'Talhur'),
(423, 1, 'Tando adam'),
(424, 1, 'Tando allahyar'),
(425, 1, 'Tando jam'),
(426, 1, 'Tando mohd khan'),
(427, 1, 'Tank'),
(428, 1, 'Tarbela'),
(429, 1, 'Tarmatan'),
(430, 1, 'Tarnol'),
(431, 1, 'Taunsa sharif'),
(432, 1, 'Taxila'),
(433, 1, 'Tharo Shah'),
(434, 1, 'Tharparkar'),
(435, 1, 'Thatta'),
(436, 1, 'Theing jattan more'),
(437, 1, 'Thull'),
(438, 1, 'Tibba sultanpur'),
(439, 1, 'Timergara'),
(440, 1, 'Tobatek singh'),
(441, 1, 'Topi'),
(442, 1, 'Toru'),
(443, 1, 'Trinda mohd pannah'),
(444, 1, 'Turbat'),
(445, 1, 'Ubaro'),
(446, 1, 'Ugoki'),
(447, 1, 'Ukba'),
(448, 1, 'Umar kot'),
(449, 1, 'Upper deval'),
(450, 1, 'Upper Dir'),
(451, 1, 'Usta mohammad'),
(452, 1, 'Utror'),
(453, 1, 'Vehari'),
(454, 1, 'Village Sunder'),
(455, 1, 'Wah cantt'),
(456, 1, 'Wahi hassain'),
(457, 1, 'Wan radha ram'),
(458, 1, 'Warah'),
(459, 1, 'Warburton'),
(460, 1, 'Washuk'),
(461, 1, 'Wazirabad'),
(462, 1, 'Yazman Mandi'),
(463, 1, 'Zahir pir'),
(464, 1, 'Zhob'),
(465, 1, 'Ziarat'),
(466, 0, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `package`
--

CREATE TABLE `package` (
  `Package_id` int(11) NOT NULL,
  `Package_title` varchar(255) DEFAULT NULL,
  `Package_description` longtext,
  `Package_Category` int(11) DEFAULT NULL,
  `Package_price` bigint(20) UNSIGNED DEFAULT NULL,
  `Vendor_id` int(11) DEFAULT NULL,
  `IsActive` bit(1) DEFAULT b'1',
  `IsDeleted` bit(1) DEFAULT b'0',
  `CreatedOn` datetime DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `package`
--

INSERT INTO `package` (`Package_id`, `Package_title`, `Package_description`, `Package_Category`, `Package_price`, `Vendor_id`, `IsActive`, `IsDeleted`, `CreatedOn`, `ModifiedOn`) VALUES
(1, 'qqqqqqqqqq', 'aaaaaaaaaaaaaaaaaaaaa', 1, 23, 1, b'1', b'0', '2018-05-29 23:13:55', NULL),
(2, 'Catering Menu', 'Return to site	\r\nWedding Ceremony Description Step by Step\r\n\r\nMay 12, 2017\r\nThis page explains a typical day-of ceremony activities. It will help you visualize the ceremony, and it will give you ideas on what you might want to customize.\r\n\r\n \r\n\r\nWhatever you wish for your ceremony we will help you do. We\'ve done literally hundreds of weddings and can confidently say that we are very, very good at it. While all ceremonies follow a certain structure, anything is possible - your imagination is the limit. But then, you have us, so there is no limit. (We\'re also working on our modesty... but it\'s not easy). More tips and ideas further below, but first here is the engagement process for officiants', 3, 1234, 1, b'1', b'0', '2018-05-29 23:17:36', NULL),
(3, 'I will arrange a wedding hall for you', 'adfsssssssssssssssssssssssssssssss', 1, 213, 1, b'1', b'0', '2018-06-04 12:46:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `package_picture`
--

CREATE TABLE `package_picture` (
  `package_picture_id` int(10) UNSIGNED NOT NULL,
  `package_id` int(11) DEFAULT NULL,
  `package_picture_path` varchar(500) DEFAULT NULL,
  `Is_Deleted` bit(1) DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `package_picture`
--

INSERT INTO `package_picture` (`package_picture_id`, `package_id`, `package_picture_path`, `Is_Deleted`) VALUES
(1, 1, 'package_1_0.jpg', b'1'),
(2, 1, 'package_1_1.jpg', b'1'),
(3, 2, 'package_2_0.jpg', b'0'),
(4, 1, 'package_1_01.jpg', b'1'),
(5, 1, 'package_1_1.jpg', b'1'),
(6, 1, 'package_1_0.jpg', b'0'),
(7, 1, 'package_1_1.jpg', b'0'),
(8, 1, 'package_1_01.jpg', b'0'),
(9, 3, 'package_3_0.jpg', b'1'),
(10, 3, 'package_3_1.jpg', b'0'),
(11, 3, 'package_3_2.jpg', b'1'),
(12, 3, 'package_3_01.jpg', b'1'),
(13, 3, 'package_3_0.jpg', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `Service_id` int(10) UNSIGNED NOT NULL,
  `Service_title` varchar(50) DEFAULT NULL,
  `Vendor_type_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`Service_id`, `Service_title`, `Vendor_type_id`) VALUES
(1, 'How much minimum people can accomodate?', 1),
(2, 'How much maximum people can accomodate?', 1),
(3, 'Do you have a hall or outdoor places\r?', 1),
(4, 'Do you have a parking space\r\n?', 1),
(5, 'Do you have restrooms/washrooms?', 1),
(6, 'Is ther any room for bride?', 1),
(7, 'Do you offer tenting service?', 1),
(8, 'Any type of cancellation policy?', 1),
(9, 'Do you have First Aid Box?', 1),
(10, 'Do you have a Food Authority license?', 4),
(11, 'Rate your Food Quality between 1-5? \r\n', 4),
(12, 'Ratio of servers to guest\r\n?', 4),
(13, 'Any type of Cancellation Policy?', 4),
(14, 'What Type of Music of you play?', 3),
(15, 'Any smoke or lighting effects\r\n?', 3),
(16, 'How much hours included in package? \r\n', 3),
(17, 'Any backup plan in case of equipement malfunction?', 3),
(18, 'Any type of Cancellation Policy?', 3),
(19, 'Do you work on user location or yours?\r\n', 7),
(20, 'Any type of Cancellation Policy?', 7),
(21, 'How much guest will come out with user?', 7),
(22, 'Do you have First Aid Box?', 7),
(23, 'Do you charge per milage or hour?', 9),
(24, 'Any type of Cancellation Policy?', 9),
(25, 'Does your Car has an auto Insurance\r\n?', 9),
(26, 'Is the Driver available?\r\n', 9),
(27, 'Do you have First Aid Box?', 9),
(28, 'How much time you need to setup?', 2),
(29, 'Any smoke or lighting effects\r\n?', 2),
(30, 'How much hours included in package? \r\n', 2),
(31, 'Any backup plan in case of equipement malfunction?', 2),
(32, 'Any type of Cancellation Policy?', 2),
(33, 'Do you offer fresh flowers?\r\n', 2),
(34, 'What is your camera quality?', 5),
(35, 'Did you just Photoshoot Or Film?', 5),
(36, 'Are you a Videographer?', 5),
(37, 'Photography style (traditional, photojournalistic)', 5),
(38, 'Any backup plan in case of equipement malfunction?', 5),
(39, 'Deliver of photos with album in no of days?', 5),
(40, 'Copyright issue.\r\n', 5);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `User_id` int(11) NOT NULL,
  `User_fname` varchar(255) DEFAULT NULL,
  `User_lname` varchar(255) DEFAULT NULL,
  `User_email` varchar(255) DEFAULT NULL,
  `User_phone_no` bigint(11) UNSIGNED ZEROFILL DEFAULT NULL,
  `User_password` varchar(128) DEFAULT NULL,
  `User_contact_preference` int(11) DEFAULT NULL,
  `CreatedBy` int(11) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ModifiedBy` int(11) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL,
  `isDeleted` int(1) DEFAULT '0',
  `IsVendor` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`User_id`, `User_fname`, `User_lname`, `User_email`, `User_phone_no`, `User_password`, `User_contact_preference`, `CreatedBy`, `CreatedOn`, `ModifiedBy`, `ModifiedOn`, `isDeleted`, `IsVendor`) VALUES
(1, 'Tayyab ', 'Ahmed', 'test@gmail.com', 03325020315, '9d9b8c76c5ed1d5220b677f067500bd9', 1, NULL, '2018-05-28 13:27:22', NULL, NULL, 0, 1),
(2, 'Test', 'Ahmed', 'abc@gmail.com', 03325020125, '9d9b8c76c5ed1d5220b677f067500bd9', 1, NULL, '2018-06-14 10:10:15', NULL, NULL, 0, 1),
(4, 'Faysal', 'Mahamud', 'faysal.turjo@gmail.com', 03325020315, '9d9b8c76c5ed1d5220b677f067500bd9', 1, NULL, '2018-10-01 14:48:36', NULL, NULL, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE `user_type` (
  `User_type_id` int(11) NOT NULL,
  `USer_type_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `Vendor_id` int(11) NOT NULL,
  `Vendor_name` varchar(255) DEFAULT NULL,
  `Vendor_type` int(11) DEFAULT NULL,
  `Vendor_description` longtext,
  `Vendor_address` varchar(255) DEFAULT NULL,
  `CreatedBy` int(11) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ModifiedBy` int(11) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL,
  `isDeleted` int(1) DEFAULT '0',
  `City` varchar(50) DEFAULT NULL,
  `Vendor_lat` double DEFAULT NULL,
  `Vendor_long` double DEFAULT NULL,
  `User_id` int(11) DEFAULT NULL,
  `isBanquetHall` int(1) DEFAULT NULL,
  `isDecorators` int(1) DEFAULT NULL,
  `isDj` int(1) DEFAULT NULL,
  `isCatering` int(1) DEFAULT NULL,
  `isPhotographer` int(1) DEFAULT NULL,
  `isBakeries` int(1) DEFAULT NULL,
  `isBridalSaloon` int(1) DEFAULT NULL,
  `isInvitation` int(1) DEFAULT NULL,
  `isLimousine` int(1) DEFAULT NULL,
  `isFlorist` int(1) DEFAULT NULL,
  `Vendor_starting_price` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`Vendor_id`, `Vendor_name`, `Vendor_type`, `Vendor_description`, `Vendor_address`, `CreatedBy`, `CreatedOn`, `ModifiedBy`, `ModifiedOn`, `isDeleted`, `City`, `Vendor_lat`, `Vendor_long`, `User_id`, `isBanquetHall`, `isDecorators`, `isDj`, `isCatering`, `isPhotographer`, `isBakeries`, `isBridalSaloon`, `isInvitation`, `isLimousine`, `isFlorist`, `Vendor_starting_price`) VALUES
(1, 'Tayyab Hall', NULL, 'ddaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'Khursheed Alam Road, Falcon Complex, Lahore, Punjab, Pakistan', NULL, '2018-05-28 21:44:39', NULL, '2018-06-04 12:36:47', 0, 'Lahore', 31.504214566890944, 74.33143593652346, 1, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(2, 'mughal e azam', NULL, 'we are a team of professional event planners.', 'Kalma Underpass, Lahore, Punjab, Pakistan', NULL, '2018-06-14 10:19:40', NULL, '2018-06-14 10:21:15', 0, 'Lahore', 31.504214566890944, 74.33143593652346, 2, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `vendor_faq`
--

CREATE TABLE `vendor_faq` (
  `Vendor_service_id` int(10) UNSIGNED NOT NULL,
  `Vendor_id` int(11) DEFAULT NULL,
  `faq_id` int(11) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `isdeleted` bit(1) DEFAULT b'0',
  `result` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor_faq`
--

INSERT INTO `vendor_faq` (`Vendor_service_id`, `Vendor_id`, `faq_id`, `creation_date`, `isdeleted`, `result`) VALUES
(1, 1, 14, '2018-05-28 21:44:39', b'0', 'qqqq'),
(2, 1, 15, '2018-05-28 21:44:39', b'0', 'wwww'),
(3, 1, 16, '2018-05-28 21:44:39', b'0', '3'),
(4, 1, 17, '2018-05-28 21:44:39', b'0', 'ddd'),
(5, 1, 18, '2018-05-28 21:44:39', b'0', ' vvvv'),
(6, 2, 1, '2018-06-14 10:19:40', b'0', '500'),
(7, 2, 2, '2018-06-14 10:19:40', b'0', '1000'),
(8, 2, 3, '2018-06-14 10:19:40', b'0', 'Hall'),
(9, 2, 4, '2018-06-14 10:19:40', b'0', 'Yes'),
(10, 2, 5, '2018-06-14 10:19:40', b'0', 'Yes'),
(11, 2, 6, '2018-06-14 10:19:40', b'0', 'Yes'),
(12, 2, 7, '2018-06-14 10:19:41', b'0', 'Yes'),
(13, 2, 8, '2018-06-14 10:19:41', b'0', 'No'),
(14, 2, 9, '2018-06-14 10:19:41', b'0', 'Yes'),
(15, 2, 14, '2018-06-14 10:19:41', b'0', 'Party'),
(16, 2, 15, '2018-06-14 10:19:41', b'0', 'Both'),
(17, 2, 16, '2018-06-14 10:19:41', b'0', '4'),
(18, 2, 17, '2018-06-14 10:19:41', b'0', 'yes'),
(19, 2, 18, '2018-06-14 10:19:41', b'0', 'No'),
(20, 4, 14, '2018-06-20 16:14:15', b'0', 'Single'),
(21, 4, 15, '2018-06-20 16:14:15', b'0', 'wwww'),
(22, 4, 16, '2018-06-20 16:14:15', b'0', '3'),
(23, 4, 17, '2018-06-20 16:14:15', b'0', 'ddd'),
(24, 4, 18, '2018-06-20 16:14:15', b'0', ' vvvv'),
(25, 5, 14, '2018-06-22 10:55:45', b'0', 'Single'),
(26, 5, 15, '2018-06-22 10:55:45', b'0', 'wwww'),
(27, 5, 16, '2018-06-22 10:55:45', b'0', '3'),
(28, 5, 17, '2018-06-22 10:55:45', b'0', 'ddd'),
(29, 5, 18, '2018-06-22 10:55:45', b'0', ' vvvv'),
(30, 6, 28, '2018-08-20 14:30:03', b'0', 'sdf'),
(31, 6, 29, '2018-08-20 14:30:03', b'0', 'yes'),
(32, 6, 30, '2018-08-20 14:30:03', b'0', 'dfsg'),
(33, 6, 31, '2018-08-20 14:30:03', b'0', 'fds'),
(34, 6, 32, '2018-08-20 14:30:03', b'0', 'df'),
(35, 6, 33, '2018-08-20 14:30:03', b'0', 'ds');

-- --------------------------------------------------------

--
-- Table structure for table `vendor_pictures`
--

CREATE TABLE `vendor_pictures` (
  `Vendor_picture_id` int(10) UNSIGNED NOT NULL,
  `Vendor_id` int(11) DEFAULT NULL,
  `Vendor_picture_path` varchar(500) DEFAULT NULL,
  `Is_profile_pic` bit(1) DEFAULT b'0',
  `Is_Deleted` bit(1) DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor_pictures`
--

INSERT INTO `vendor_pictures` (`Vendor_picture_id`, `Vendor_id`, `Vendor_picture_path`, `Is_profile_pic`, `Is_Deleted`) VALUES
(1, 1, 'vendor_1.jpg', b'1', b'0'),
(2, 1, 'vendor_11.jpg', b'1', b'0'),
(3, 1, 'vendor_12.jpg', b'1', b'0'),
(4, 1, 'vendor_13.jpg', b'1', b'0'),
(5, 2, 'vendor_2.jpg', b'1', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `vendor_service`
--

CREATE TABLE `vendor_service` (
  `Vendor_service_id` int(10) UNSIGNED NOT NULL,
  `Vendor_id` int(11) DEFAULT NULL,
  `Service_id` int(11) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `isdeleted` bit(1) DEFAULT b'0',
  `result` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor_service`
--

INSERT INTO `vendor_service` (`Vendor_service_id`, `Vendor_id`, `Service_id`, `creation_date`, `isdeleted`, `result`) VALUES
(1, 1, 14, '2018-05-28 21:44:39', b'0', 'Party'),
(2, 1, 15, '2018-05-28 21:44:39', b'0', 'Both'),
(3, 1, 16, '2018-05-28 21:44:39', b'0', '4'),
(4, 1, 17, '2018-05-28 21:44:39', b'0', 'yes'),
(5, 1, 18, '2018-05-28 21:44:39', b'0', 'No'),
(6, 2, 1, '2018-06-14 10:19:40', b'0', '500'),
(7, 2, 2, '2018-06-14 10:19:40', b'0', '1000'),
(8, 2, 3, '2018-06-14 10:19:40', b'0', 'Hall'),
(9, 2, 4, '2018-06-14 10:19:40', b'0', 'Yes'),
(10, 2, 5, '2018-06-14 10:19:40', b'0', 'Yes'),
(11, 2, 6, '2018-06-14 10:19:40', b'0', 'Yes'),
(12, 2, 7, '2018-06-14 10:19:41', b'0', 'Yes'),
(13, 2, 8, '2018-06-14 10:19:41', b'0', 'No'),
(14, 2, 9, '2018-06-14 10:19:41', b'0', 'Yes'),
(15, 2, 14, '2018-06-14 10:19:41', b'0', 'Party'),
(16, 2, 15, '2018-06-14 10:19:41', b'0', 'Both'),
(17, 2, 16, '2018-06-14 10:19:41', b'0', '4'),
(18, 2, 17, '2018-06-14 10:19:41', b'0', 'yes'),
(19, 2, 18, '2018-06-14 10:19:41', b'0', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `vendor_type`
--

CREATE TABLE `vendor_type` (
  `Vendor_type_id` int(11) NOT NULL,
  `Vendor_type_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor_type`
--

INSERT INTO `vendor_type` (`Vendor_type_id`, `Vendor_type_name`) VALUES
(1, 'Banquet Hall'),
(2, 'Decorators'),
(3, 'Dj'),
(4, 'Catering'),
(5, 'Photographer'),
(6, 'Bakeries'),
(7, 'Bridal Saloon'),
(8, 'Invitation'),
(9, 'Limousine'),
(10, 'Florist');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`Event_id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD KEY `Service_id` (`faq_id`);

--
-- Indexes for table `lb_city`
--
ALTER TABLE `lb_city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `package`
--
ALTER TABLE `package`
  ADD KEY `Package_id` (`Package_id`);

--
-- Indexes for table `package_picture`
--
ALTER TABLE `package_picture`
  ADD KEY `package_picture_id` (`package_picture_id`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD KEY `Service_id` (`Service_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`User_id`);

--
-- Indexes for table `user_type`
--
ALTER TABLE `user_type`
  ADD PRIMARY KEY (`User_type_id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD KEY `Vendor_id` (`Vendor_id`);

--
-- Indexes for table `vendor_faq`
--
ALTER TABLE `vendor_faq`
  ADD KEY `Vendor_service_id` (`Vendor_service_id`);

--
-- Indexes for table `vendor_pictures`
--
ALTER TABLE `vendor_pictures`
  ADD KEY `Vendor_picture_id` (`Vendor_picture_id`);

--
-- Indexes for table `vendor_service`
--
ALTER TABLE `vendor_service`
  ADD KEY `Vendor_service_id` (`Vendor_service_id`);

--
-- Indexes for table `vendor_type`
--
ALTER TABLE `vendor_type`
  ADD PRIMARY KEY (`Vendor_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=467;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `Event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `faq_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `lb_city`
--
ALTER TABLE `lb_city`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=467;
--
-- AUTO_INCREMENT for table `package`
--
ALTER TABLE `package`
  MODIFY `Package_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `package_picture`
--
ALTER TABLE `package_picture`
  MODIFY `package_picture_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `Service_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `User_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user_type`
--
ALTER TABLE `user_type`
  MODIFY `User_type_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `Vendor_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `vendor_faq`
--
ALTER TABLE `vendor_faq`
  MODIFY `Vendor_service_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `vendor_pictures`
--
ALTER TABLE `vendor_pictures`
  MODIFY `Vendor_picture_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `vendor_service`
--
ALTER TABLE `vendor_service`
  MODIFY `Vendor_service_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `vendor_type`
--
ALTER TABLE `vendor_type`
  MODIFY `Vendor_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
